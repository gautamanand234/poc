package com.example.handlerthread.strategy;

public class Multiplication implements MathematicalStrategy{
    @Override
    public int sum(int a, int b) {
        return a*b;
    }
}
