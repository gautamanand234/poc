package com.example.handlerthread.Decorator;

public class ColuredRectangle extends DecoratorShapeColured{
    public ColuredRectangle(Shape shape) {
        super(shape);
    }

    @Override
    public void draw() {
        shape.draw();
        System.out.println("This is colured Rectangle");
    }
}
