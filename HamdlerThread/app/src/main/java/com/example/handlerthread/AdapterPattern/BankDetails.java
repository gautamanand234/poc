package com.example.handlerthread.AdapterPattern;

import kotlin.jvm.JvmField;

public class BankDetails {

    private String accNo;
    private String bankName;
    private long amount;

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
