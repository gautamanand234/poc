package com.example.handlerthread;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.handlerthread.AdapterPattern.BankCustomer;
import com.example.handlerthread.Decorator.Circle;
import com.example.handlerthread.Decorator.ColuredRectangle;
import com.example.handlerthread.Decorator.Shape;
import com.example.handlerthread.Observor.Observable2;
import com.example.handlerthread.Observor.Observor1;
import com.example.handlerthread.sample.New;
import com.example.handlerthread.sample.REpo;
import com.example.handlerthread.sample.RectangleShape;
import com.example.handlerthread.sample.ShapesFactory;
import com.example.handlerthread.strategy.Addition;
import com.example.handlerthread.strategy.COntext;
import com.example.handlerthread.strategy.Multiplication;

public class MainActivity extends AppCompatActivity {
    MyHandlerThread myHandlerThread;
    TextView textView;
    static int count = 0;
    Handler  handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       /* myHandlerThread = new MyHandlerThread("HandlerThread");
        myHandlerThread.start();
        textView = findViewById(R.id.tv);
        handler=new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                if (msg.what==145){
                    textView.setText(count+" Sent Message from other thread");
                }
            }
        };
        myHandlerThread.setUiHandler(handler);
        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                textView.setText(count+"");
                myHandlerThread.addMessage(1);
            }
        });
*/

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShapesFactory shapesFactory=new ShapesFactory();
                shapesFactory.getShapes("asd");
            }
        });



        /*BankCustomer  bankCustomer=new BankCustomer();
        bankCustomer.setBankDetails("32424124","ICICI",90000);
        System.out.println("bankCustomer: "+bankCustomer.getCreditCard());
*/

     /*   Shape shape=new Circle();
        ColuredRectangle coluredRectangle=new ColuredRectangle(shape);
        coluredRectangle.draw();*/
      /*  new Observor1();
        Observable2 observable2=new Observable2();
        //observable2.run();

        Thread thread=new Thread(observable2 );
        thread.start();*/

       /* COntext cOntext=new COntext(new Addition());
        System.out.println("Addition: "+ cOntext.execute(5,7));

        COntext cOntext1=new COntext(new Multiplication());
        System.out.println("Multiplication: "+ cOntext1.execute(5,7));*/


      //  RectangleShape rectangleShape=new RectangleShape();
       // rectangleShape.getSum(7,8);


    }


}

