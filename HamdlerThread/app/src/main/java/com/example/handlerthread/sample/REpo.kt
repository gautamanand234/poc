package com.example.handlerthread.sample

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class REpo {
    val arrayStr = arrayListOf<String>("one", "two", "three", "four")
    suspend fun getProducer(): Flow<String> {
        return flow<String> {
            for (i in 1..10 step 2) {
                emit(i.toString())
                delay(100)
            }
        }
    }

}