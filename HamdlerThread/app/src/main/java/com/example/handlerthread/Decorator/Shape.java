package com.example.handlerthread.Decorator;

public interface Shape {
    void draw();
}
