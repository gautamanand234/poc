package com.example.handlerthread.AdapterPattern;

public class BankCustomer extends BankDetails implements Credit {
    @Override
    public void setBankDetails(String accNo,
                               String bankName
            , long amount) {

        setAccNo(accNo);
        setAmount(amount);
        setBankName(bankName);

    }

    @Override
    public String getCreditCard() {
        return getAccNo() + "/" + getBankName() + "/" + getAmount();
    }
}
