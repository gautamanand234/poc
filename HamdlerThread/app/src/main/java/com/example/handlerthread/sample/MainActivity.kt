package com.example.handlerthread.sample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking


class MainActivity : AppCompatActivity() {
    val names = listOf<String>("ahem", "rashi", "kokila")
    private var mFirebaseAnalytics: FirebaseAnalytics? = null
    val namesFlow = flow {
        for (name in names) {
            emit(name)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        getName()
        runBlocking {
            namesFlow.
            map { value -> value.length>2 }.
            collect { print(it) }
            val bundle = Bundle()
            mFirebaseAnalytics?.logEvent(FirebaseAnalytics.Param.ACHIEVEMENT_ID,bundle)

        }

    }

    fun getName() {

    }
}