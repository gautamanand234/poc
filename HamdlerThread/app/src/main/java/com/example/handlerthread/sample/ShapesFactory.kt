 package com.example.handlerthread.sample

import com.example.handlerthread.AdapterPattern.BankCustomer
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

 class ShapesFactory : RectangleShape() {
    var a: String = "a"
    var b = "Das"
    val aa: Map<Int, Int> = mapOf(1 to 1, 2 to 3);

    fun getShapes(ff: Any) {
        getSum(4, 5)
        aa?.let {
            it[4] to 5

        } ?: mapOf(6 to 9)

        b = a.also { a = b }//  swapping two numbers

        with(aa){
            aa[1].takeUnless { b === "sa"}

        }
        val shapesFactory=BankCustomer().apply {
            accNo="44444"
        }

        print(shapesFactory.toString())


        when (ff) {
            1 -> print("this value is 1")
            is String -> print("vslu is string")
        }

    }

    fun getList(){
         val reo:   REpo  = REpo();

        GlobalScope.launch {reo.getProducer().collect{
            print("")
        }  }


    }

}