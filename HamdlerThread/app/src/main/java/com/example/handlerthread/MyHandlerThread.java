package com.example.handlerthread;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

public class MyHandlerThread extends HandlerThread {
    CustomHandler customHandler;
    static Handler uiHandler;

    public MyHandlerThread(String name) {
        super(name);
    }


    public void setUiHandler(Handler uiHandler6) {
        uiHandler = uiHandler6;
    }

    @Override
    protected void onLooperPrepared() {
        super.onLooperPrepared();
        customHandler = new CustomHandler(getLooper());
    }

    public void addMessage(int msg) {
        if (customHandler != null)
            customHandler.sendEmptyMessage(msg);
    }


    private static class CustomHandler extends Handler {
        public CustomHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 1: {
                    uiHandler.sendEmptyMessage(145);
                    System.out.println("Message 1 is printed");
                    break;
                }
                case 2: {
                    System.out.println("Message 2 is printed");
                    break;
                }
            }
        }
    }
}
