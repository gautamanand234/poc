package com.example.handlerthread.Observor;

import java.util.Observable;

public class Observable2 extends Observable implements Runnable {

    @Override
    public void run() {
        String msg = "This is observable 2";
        setChanged();
        notifyObservers(msg);
    }
}
