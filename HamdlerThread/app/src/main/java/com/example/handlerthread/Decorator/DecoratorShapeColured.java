package com.example.handlerthread.Decorator;

public abstract class DecoratorShapeColured implements Shape {
    Shape shape;

    public DecoratorShapeColured(Shape shape) {
        this.shape = shape;
    }

    @Override
    public void draw() {
        shape.draw();
    }
}
