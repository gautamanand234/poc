package com.example.handlerthread.AdapterPattern;

public interface Credit {
    public void setBankDetails(String accNo,
                               String bankName
            , long amount);
    public String getCreditCard();
}
