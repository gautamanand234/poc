package com.example.handlerthread.strategy;

public interface MathematicalStrategy {
     int sum(int a, int b);
}
