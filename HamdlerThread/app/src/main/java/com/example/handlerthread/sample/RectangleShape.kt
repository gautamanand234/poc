package com.example.handlerthread.sample

abstract class RectangleShape {

    fun getSum(a: Int, b: Int) = a + b
}