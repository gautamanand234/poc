package com.example.handlerthread.strategy;

import com.google.android.material.circularreveal.CircularRevealHelper;

public class COntext {
    MathematicalStrategy strategy;

    public COntext(MathematicalStrategy strategy) {
        this.strategy = strategy;
    }

    public int execute(int a, int b) {
        return strategy.sum(a, b);
    }
}
