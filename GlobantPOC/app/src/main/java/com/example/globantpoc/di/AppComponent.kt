package com.example.globantpoc.di

import android.app.Application
import com.example.globantpoc.GlobantPOCApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class,ActivityBuilder::class,AppModule::class,NetworkModule::class])
interface AppComponent {
    fun inject(myApplication: GlobantPOCApp)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun myApp(application: Application): Builder

        fun build(): AppComponent
    }
}