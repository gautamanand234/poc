package com.example.globantpoc

import android.app.Activity
import android.app.Application
import com.example.globantpoc.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector

import dagger.android.HasAndroidInjector
import javax.inject.Inject

class GlobantPOCApp : Application(), HasAndroidInjector{

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> {
        return activityDispatchingAndroidInjector
    }

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder()
            .myApp(this)
            .build()
            .inject(this)

    }


}