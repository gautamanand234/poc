package com.example.globantpoc.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.globantpoc.BuildConfig
import com.example.globantpoc.databinding.ListItemAlbumBinding
import com.example.globantpoc.model.AlbumArt
import com.squareup.picasso.Picasso
import javax.inject.Inject


class AlbumListAdapter @Inject constructor() : RecyclerView.Adapter<AlbumListAdapter.AlbumViewHolder>() {
    private val data = mutableListOf<AlbumArt>()

    fun submitList(albums: List<AlbumArt>) {
        data.addAll(albums)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumViewHolder =
        AlbumViewHolder(
            ListItemAlbumBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        )

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
        with(holder){
            with(data[position]){
                val builder = Picasso.Builder(itemView.context)
                builder.listener { _piccaso, uri, exception ->
                    if (BuildConfig.DEBUG && exception!=null)
                        Log.e("imageview", this.thumbnailUrl, exception)
                }.build()
                    .load(this.thumbnailUrl)
                    .placeholder(android.R.drawable.ic_dialog_map)
                    .into(viewBinding.albumArt)

                viewBinding.tvAlbum.text=this.title

            }
        }
    }


    inner class AlbumViewHolder(val viewBinding:  ListItemAlbumBinding) : RecyclerView.ViewHolder(viewBinding.root)

}


