package com.example.globantpoc.ui.view

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.globantpoc.databinding.ActivityMainBinding
import com.example.globantpoc.model.AlbumArt
import com.example.globantpoc.ui.adapter.AlbumListAdapter
import com.example.globantpoc.ui.viewmodel.AlbumViewModel
import com.example.globantpoc.ui.viewmodel.ViewModelProviderFactory
import com.example.globantpoc.utils.Status
import dagger.android.AndroidInjection
import javax.inject.Inject


class AlbumViewActivity : AppCompatActivity() {
    @Inject
    lateinit var factory: ViewModelProviderFactory

    private lateinit var albumViewModel: AlbumViewModel

    private lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var albumListAdapter: AlbumListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        albumViewModel= ViewModelProvider(this, factory)[AlbumViewModel::class.java]
        setSupportActionBar(binding.toolbar)
        setupUI()
        setupObserverAlbumArt()
    }
    private fun setupUI() {
        binding.llRecycler.albumList.run {
            layoutManager = LinearLayoutManager(context)
            itemAnimator=DefaultItemAnimator()
            adapter=albumListAdapter
            addItemDecoration(
            DividerItemDecoration(
                context,
                (layoutManager as LinearLayoutManager).orientation
            )
        ) }

    }
    private fun setupObserverAlbumArt()  {
        albumViewModel.getAlbums().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    Log.d("View","Response SUCCESS")
                    it.data?.let { input -> renderList(input) }
                    binding.llRecycler.albumList.visibility = View.VISIBLE
                }
                Status.LOADING -> {
                    Log.d("View","Response LOADING")
                    binding.llRecycler.albumList.visibility = View.GONE
                }
                Status.ERROR -> {
                    Log.d("View","Response ERROR")
                    //Handle Error
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun renderList(users: List<AlbumArt>) {
        albumListAdapter.submitList(users)
        albumListAdapter.notifyDataSetChanged()
    }
}