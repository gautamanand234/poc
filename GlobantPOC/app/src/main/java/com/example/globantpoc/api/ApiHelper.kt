package com.example.globantpoc.api

import com.example.globantpoc.model.AlbumArt

import retrofit2.Response

interface ApiHelper {

    suspend fun getAlbumArt(): Response<List<AlbumArt>>

}