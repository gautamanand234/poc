package com.example.globantpoc.api

import com.example.globantpoc.model.AlbumArt
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {
    @GET("photos/")
    suspend fun getAlbumArt(): Response<List<AlbumArt>>
}