package com.example.globantpoc.di

import android.app.Application
import android.content.Context
import com.example.globantpoc.GlobantPOCApp
import com.example.globantpoc.utils.NetworkHelper
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
 class AppModule {

    @Provides
    @Singleton
    fun provideNetworkHelper(context: Application?) = NetworkHelper(context?.applicationContext)



}