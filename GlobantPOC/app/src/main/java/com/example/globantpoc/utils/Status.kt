package com.example.globantpoc.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}