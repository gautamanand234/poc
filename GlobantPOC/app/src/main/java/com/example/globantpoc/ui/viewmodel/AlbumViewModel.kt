package com.example.globantpoc.ui.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.globantpoc.api.ApiHelper
import com.example.globantpoc.model.AlbumArt
import com.example.globantpoc.utils.NetworkHelper
import com.example.globantpoc.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AlbumViewModel constructor(
    private val apiHelper: ApiHelper,
    private val networkHelper: NetworkHelper
) : ViewModel() {
     private val albumArts = MutableLiveData<Resource<List<AlbumArt>>>()

    init {
        fetchAlbums()
    }

    fun getAlbums(): LiveData<Resource<List<AlbumArt>>> {
        return albumArts
    }

    private fun fetchAlbums() {
        viewModelScope.launch {
            if (networkHelper.isNetworkConnected) {
                albumArts.postValue(Resource.loading(null))
                apiHelper.getAlbumArt().let {
                    if (it.isSuccessful) {
                        Log.d("View", "Response success internal")
                        albumArts.postValue(Resource.success(it.body()))
                    } else albumArts.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else albumArts.postValue(Resource.error("No internet connection", null))
        }
    }

    private fun fetchAlbum() {


      /*  liveData(Dispatchers.IO) {
            if (networkHelper.isNetworkConnected) {
                albumArts.postValue(Resource.loading(null))
                apiHelper.getAlbumArt().let {
                    if (it.isSuccessful) {
                        Log.d("View", "Response success internal")
                        albumArts.postValue(Resource.success(it.body()))
                    } else albumArts.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else albumArts.postValue(Resource.error("No internet connection", null))
        }*/
    }

//livedatascope
    //mvvm mvp
    //dalay and
    //repository
    //unit
    //class level logs in
    ///give examples
    ///dagger qualifiers
// why di is needed
}