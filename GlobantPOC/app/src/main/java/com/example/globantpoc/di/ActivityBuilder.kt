package com.example.globantpoc.di

import com.example.globantpoc.ui.view.AlbumViewActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): AlbumViewActivity
}