package com.example.globantpoc.api

import com.example.globantpoc.model.AlbumArt
import retrofit2.Response

class ApiHelperImpl(private val apiService: ApiService):ApiHelper {

    override suspend fun getAlbumArt(): Response<List<AlbumArt>> =apiService.getAlbumArt()

}