package com.example.globantpoc.ui.viewmodel

import android.accounts.Account
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.globantpoc.api.ApiHelper
import com.example.globantpoc.model.AlbumArt
import com.example.globantpoc.utils.NetworkHelper
import com.example.globantpoc.utils.Resource
import com.example.globantpoc.utils.Status
import com.example.globantpoc.utils.TestCoroutineRule
import okhttp3.MediaType
import okhttp3.Response
import okhttp3.ResponseBody
import org.json.JSONObject
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner


@kotlinx.coroutines.ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class AlbumViewModelTest {
    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var apiHelper: ApiHelper

    @Mock
    private lateinit var networkHelper: NetworkHelper

    @Mock
    private lateinit var apiUsersObserver: Observer<Resource<List<AlbumArt>>>

    @Before
    fun setUp() {
    }



    @Test
    fun givenServerResponseError_whenFetch_shouldReturnError() {
        testCoroutineRule.runBlockingTest {
            doReturn(true).`when`(networkHelper).isNetworkConnected
            val httpResponse = mock(Response::class.java)
            val aResponse: retrofit2.Response<List<AlbumArt>> = retrofit2.Response.error(
                ResponseBody.create(
                    null,
                    "{\"key\":[\"somestuff\"]}"
                ),
                httpResponse
            )
            val viewModel = AlbumViewModel(apiHelper, networkHelper)
            viewModel.getAlbums().observeForever(apiUsersObserver)
            verify(apiHelper).getAlbumArt()
            verify(apiUsersObserver).onChanged(Resource(status = Status.ERROR, data = null, any()))
            viewModel.getAlbums().removeObserver(apiUsersObserver)
        }
    }

    @Test
    fun givenNetworkError_whenFetch_shouldReturnError() {
        testCoroutineRule.runBlockingTest {
            doReturn(false).`when`(networkHelper).isNetworkConnected
            val errorMessage = "No internet connection"
            val viewModel = AlbumViewModel(apiHelper, networkHelper)
            viewModel.getAlbums().observeForever(apiUsersObserver)
            verify(apiUsersObserver).onChanged(
                Resource.error(
                    errorMessage,
                    null
                )
            )
            viewModel.getAlbums().removeObserver(apiUsersObserver)
        }
    }
}